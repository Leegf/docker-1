docker build -t cpp_env .
docker run -it --rm -e COLUMNS="`tput cols`" -e LINES="`tput lines`" --cap-add=SYS_PTRACE --security-opt seccomp=unconfined --mount type=bind,source="$HOME",target=/project cpp_env
