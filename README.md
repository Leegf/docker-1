**00_how_to_docker** folder contains solutions to some basic docker problems and testing its functionality.

**01_dockerfiles** contains examples of different dockerfiles.

**02_bonus** contains another examples of dockerfiles, which might be useful for my own other projects.